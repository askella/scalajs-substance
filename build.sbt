lazy val root = project.in(file(".")).
  enablePlugins(ScalaJSPlugin)

name := "Scala.js Substance Facade"

normalizedName := "scalajs-substance"

version := "1.0.0-beta3"

organization := "co.askella"

scalaVersion := "2.11.7"

crossScalaVersions := Seq("2.10.6", "2.11.7")

libraryDependencies +=
  "org.scala-js" %%% "scalajs-dom" % "0.9.0"

scalacOptions ++= Seq("-deprecation", "-feature", "-Xfatal-warnings")

homepage := Some(url("http://scala-js.org/"))

licenses += ("BSD 3-Clause", url("http://opensource.org/licenses/BSD-3-Clause"))

scmInfo := Some(ScmInfo(
    url("https://bitbucket.org/askella/scalajs-substance"),
    "scm:git:git@bitbucket.org:askella/scalajs-substance.git",
    Some("git@bitbucket.org:askella/scalajs-substance.git")))

publishMavenStyle := true

publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "service/local/staging/deploy/maven2")
}

pomExtra := (
  <developers>
    <developer>
      <id>sjrd</id>
      <name>Rakshith Begane</name>
      <url>https://github.com/rakshithbegane</url>
    </developer>
  </developers>
)

pomIncludeRepository := { _ => false }
