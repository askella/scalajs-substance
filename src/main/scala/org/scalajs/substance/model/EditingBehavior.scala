package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class EditingBehavior extends js.Object{
  
  def defineMerge(firstType: js.Any, secondType: js.Any, impl: js.Any): EditingBehavior = js.native
  def canMerge(firstType: js.Any, secondType: js.Any): Boolean = js.native
  def getMerger(firstType: js.Any, secondType: js.Any): js.Any = js.native
  def defineComponentMerge(nodeType: js.Any, impl: js.Any): Unit = js.native
  def canMergeComponents(nodeType: js.Any): js.Any = js.native
  def getComponentMerger(nodeType: js.Any): js.Any = js.native
  def defineBreak(nodeType: js.Any, impl: js.Any): EditingBehavior = js.native
  def canBreak(nodeType: js.Any): js.Any = js.native
  def getBreaker(nodeType: js.Any): js.Any = js.native

}
