package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class XMLExporter(config: js.Object) extends DOMExporter(config){

  override def getDefaultBlockConverter(): js.Object = js.native
  override def getDefaultPropertyAnnotationConverter(): js.Object = js.native


}
