package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TextNode(doc: js.Any, props: js.Object) extends DocumentNode(doc, props){

  def getTextPath(): js.Array[String] = js.native
  def getText(): String = js.native
  
}

@js.native
object TextNode extends js.Object{

  val name: String = js.native
  val isText: Boolean = js.native

}