package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Range(val start: Int, val end: Int, val reverse: js.Any, val containerId: js.Any) extends js.Object{

  def isCollapsed(): Boolean = js.native
  def equals(other: Range): Boolean = js.native
  def isReverse(): Boolean = js.native
  override def toString(): String = js.native

}
