package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
trait ParentNodeMixin extends js.Object{

  def hasChildren(): Boolean = js.native
  def getChildrenProperty(): Unit = js.native
  def getChildIndex(child: js.Any): js.Any = js.native
  def getChildren(): js.Array[js.Any] = js.native
  def getChildAt(idx: Int): js.Any = js.native
  def getChildCount(): Int = js.native
  def getAddressablePropertyNames(): js.Array[js.Any] = js.native
  
}
