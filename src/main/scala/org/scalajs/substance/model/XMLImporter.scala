package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class XMLImporter(config: js.Object)  extends DOMImporter(config){

  def importDocument(xml: js.Any): js.Any = js.native
}
