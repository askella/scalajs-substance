package org.scalajs.substance.model

import org.scalajs.substance.model.data.Schema

import scala.scalajs.js
import scala.scalajs.js.{Any, native}


/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentSchema(name: String, version: String) extends Schema(name, version){

  override def getDefaultTextType(): String = js.native
  def isAnnotationType(`type`: js.Any): js.Any = js.native
  override def getBuiltIns(): js.Array[data.Node] = js.native

}
