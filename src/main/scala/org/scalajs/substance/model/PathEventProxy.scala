package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class PathEventProxy(val doc: js.Any) extends js.Object{

  def on(path: js.Any, method: js.Any, context: js.Any): Unit = js.native
  def off(context: js.Any, path: js.Any, method: js.Any): Unit = js.native
  def connect(listener: js.Any, path: js.Any, method: js.Any): Unit = js.native
  def disconnect(listener: js.Any): Unit = js.native
  def onDocumentChanged(change: js.Any, info: js.Any, doc: js.Any): js.Any = js.native
  def _add(listener: js.Any, path: js.Any, method: js.Any): Unit = js.native
  def _remove(listener: js.Any, path: js.Any, method: js.Any): Unit = js.native
  
}
