package org.scalajs.substance.model

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSName, ScalaJSDefined}

@js.native
class ContainerAnnotation(arguments: js.Any*) extends DocumentNode(???, ???){

  def getText(): String = js.native
  def getSelection(): ContainerSelection = js.native
  override def setHighlighted(highlighted: js.Any, scope: js.Any): Unit = js.native
  def updateRange(tx: js.Any, sel: Selection): Unit = js.native
  def getFragments(): js.Array[ContainerAnnotation.Fragment] = js.native
  def getStartAnchor(): js.Any = js.native
  def getEndAnchor(): js.Any = js.native

}

@js.native
object ContainerAnnotation extends ContainerAnnotation(???){

  @js.native
  class Fragment(anno: ContainerAnnotation, val path: js.Array[String], mode: js.Any) extends js.Object{

    def getTypeNames(): js.Array[String] = js.native
    def getStartOffset(): Int = js.native
    def getEndOffset(): Int = js.native

  }
}
