package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class PropertyAnnotation(arguments: js.Any*) extends DocumentNode(???, ???){
  

  def getText(): String = js.native
  def canSplit(): Boolean = js.native
  def isAnchor(): Boolean = js.native
  def getSelection(): Selection = js.native
  def updateRange(tx: js.Any, sel: Selection): Unit = js.native

}

@js.native
object PropertyAnnotation extends js.Object{

  val name: String = js.native
  val isPropertyAnnotation: Boolean = js.native

}
