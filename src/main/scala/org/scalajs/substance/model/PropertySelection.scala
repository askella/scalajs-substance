package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class PropertySelection(val path: js.Any, val startOffset: Int, val endOffset: Int,
                        val reverse: js.Any, val surfaceId: js.Any) extends Selection{

  override def toJSON(): js.Object = js.native
  override def isPropertySelection(): Boolean = js.native
  override def isNull(): Boolean = js.native
  override def isCollapsed(): Boolean = js.native
  override def isReverse(): Boolean = js.native
  def equals(other: js.Any): Boolean = js.native
  override def toString(): String = js.native
  def collapse(direction: String): PropertySelection = js.native
  def getRange(): js.Any = js.native
  def getPath(): js.Array[String] = js.native
  def getStartOffset(): Int = js.native
  def getEndOffset(): Int = js.native
  def isInsideOf(other: Selection, strict: Boolean): Boolean = js.native
  def contains(other: Selection, strict: Boolean): Boolean = js.native
  def overlaps(other: Selection, strict: Boolean): Boolean = js.native
  def isRightAlignedWith(other: Selection): Boolean = js.native
  def isLeftAlignedWith(other: Selection): Boolean = js.native
  def expand(other: Selection): Boolean = js.native
  def truncateWith(other: Selection): Selection = js.native
  def createWithNewRange(startOffset: Int, endOffset: Int): Selection = js.native
  override def getFragments(): js.Array[Selection.Fragment] = js.native

}

@js.native
object PropertySelection extends js.Object{

  def fromJSON(json: js.Object): PropertySelection = js.native

}


