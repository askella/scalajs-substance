package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class JSONConverter extends js.Object{

  def importDocument(doc: js.Any, json: js.Any): js.Any = js.native
  def exportDocument(doc: js.Any): js.Object = js.native
  
}
