package org.scalajs.substance.model

import org.scalajs.substance.model.data.NodeIndex

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentIndex extends NodeIndex with Index{

}
