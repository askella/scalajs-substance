package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class HTMLExporter(config: js.Any) extends DOMExporter(config){

  override def exportDocument(doc: js.Any): js.Any = js.native
  override def getDefaultBlockConverter(): js.Object = js.native
  override def getDefaultPropertyAnnotationConverter(): js.Object = js.native

}
