package org.scalajs.substance.model

import org.scalajs.substance.ui.DOMElement

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DOMImporter(val config: js.Any) extends js.Object{

  def reset(): Unit = js.native
  def createDocument(): Document = js.native
  def generateDocument(): Document = js.native
  def _createDocument(): Document = js.native
  def convertContainer(elements: DOMElement, containerId: String): js.Object = js.native
  def convertElement(el: DOMElement): js.Object = js.native
  def _convertElement(el: DOMElement, mode: js.Any): js.Any = js.native
  def createNode(node: js.Any): js.Any = js.native
  def show(node: js.Any): Unit = js.native
  def _createAndShow(node: js.Any): Unit = js.native
  def _nodeData(el: DOMElement, `type`: js.Any): js.Any = js.native
  def annotatedText(el: DOMElement, path: js.Array[String], options: js.Any): String = js.native
  def plainText(el: DOMElement): String = js.native
  def customText(text: String): String = js.native
  def nextId(prefix: String): String = js.native
  def getIdForElement(el: DOMElement, `type`: js.Any): js.Any = js.native
  def defaultConverter(el: DOMElement, converter: js.Any): js.Any = js.native
  def _defaultElementMatcher(el: DOMElement): js.Any = js.native
  def _annotatedText(iterator: js.Any): String = js.native
  def _getConverterForElement(el: DOMElement, mode: js.Any): js.Any = js.native
  def _wrapInlineElementsIntoBlockElement(childIterator: js.Any): DocumentNode = js.native
  def _createDefaultBlockElement(el: DOMElement): DocumentNode = js.native
  def _trimTextContent(el: js.Any): js.Any = js.native
  def _trimLeft(text: String): String = js.native
  def _trimRight(text: String): String = js.native

}

@js.native
object DOMImporter extends js.Object{

  @js.native
  class State extends  js.Object{

    def reset(): Unit = js.native
    def pushElementContext(tagName: String): Unit = js.native
    def popElementContext(): js.Any = js.native
    def getCurrentElementContext(): js.Any = js.native

  }

}