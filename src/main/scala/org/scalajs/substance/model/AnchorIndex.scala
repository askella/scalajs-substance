package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class AnchorIndex(val doc: js.Any) extends  js.Object with Index{

  def select(node: js.Any): js.Any = js.native
  def reset(data: js.Any): Unit = js.native
  def get(path: js.Any, containerName: js.Any): js.Any = js.native
  def create(containerAnno: js.Any): Unit = js.native
  def delete(containerAnno: js.Any): Unit = js.native
  def update(node: js.Any, path: js.Any, newValue: js.Any, oldValue: js.Any): Unit = js.native



}
