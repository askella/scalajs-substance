package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class CustomSelection(val data: js.Any) extends Selection{

  override def toString(): String = js.native
  override def isCustomSelection(): Boolean = js.native
  override def toJSON(): js.Object = js.native
  def equals(other: js.Any): Boolean = js.native

}

@js.native
object CustomSelection extends js.Object{
  def fromJSON(json: Option[js.Object]): CustomSelection = js.native
}