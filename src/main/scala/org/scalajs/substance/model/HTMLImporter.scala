package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class HTMLImporter(config: js.Any) extends DOMImporter(config){

  def importDocument(html: js.Any): js.Any = js.native
  def convertDocument(documentEl: js.Any): js.Any = js.native


}
