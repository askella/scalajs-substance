package org.scalajs.substance.model

import org.scalajs.substance.model.data.IncrementalData

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */

@js.native
class Document(val schema: DocumentSchema) extends js.Object{

  def addIndex(name: String, index: Index): Unit = js.native
  def getIndex(): Index = js.native
  def getSchema(): DocumentSchema = js.native
  def contains(id: js.Any): Boolean = js.native
  def get(path: js.Any): js.Any = js.native
  def getNodes(): DocumentNode = js.native
  @JSName("import")
  def importM(importer: js.Any): Unit = js.native
  def create(nodeData: js.Object): DocumentNode = js.native
  def delete(nodeId: String): DocumentNode = js.native
  def set(path: js.Array[String], value: js.Any): DocumentNode = js.native
  def update(path: js.Array[String], diff: js.Object): js.Any = js.native
  def addIndex(name: String, index: DocumentIndex): js.Any = js.native
  def getIndex(name: String): DocumentIndex = js.native
  def createSelection(): Selection = js.native
  def getEventProxy(name: js.Any): js.Any = js.native
  def newInstance(): Document = js.native
  def fromSnapshot(data: js.Any): js.Any = js.native
  def getDocumentMeta(): js.Any = js.native
  def _apply(documentChange: js.Any): Unit = js.native
  def _notifyChangeListeners(change: js.Any, info: js.Any): Unit = js.native
  def _updateEventProxies(change: js.Any, info: js.Any): Unit = js.native
  def loadSeed(seed: js.Any): Unit = js.native
  def toJSON(): js.Object = js.native
  def getTextForSelection(sel: js.Any): js.Any = js.native
  def setText(path: js.Any, text: js.Any, annotations: js.Any): Unit = js.native
  def _create(nodeData: js.Any): js.Any = js.native
  def _delete(nodeId: js.Any): js.Any = js.native
  def _update(path: js.Any, diff: js.Any): js.Any = js.native
  def _set(path: js.Any, value: js.Any): js.Any = js.native

}

