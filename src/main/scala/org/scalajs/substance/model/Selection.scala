package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Selection extends js.Object{

  def getDocument(): Document = js.native
  def attach(doc: Document): Selection = js.native
  def isNull(): Boolean = js.native
  def isPropertySelection(): Boolean = js.native
  def isContainerSelection(): Boolean = js.native
  def isNodeSelection(): Boolean = js.native
  def isCustomSelection(): Boolean = js.native
  def isTableSelection(): Boolean = js.native
  def isCollapsed(): Boolean = js.native
  def isReverse(): Boolean = js.native
  def equals(other: Selection): Boolean = js.native
  override def toString(): String = js.native
  def toJSON(): js.Object = js.native
  def getFragments(): js.Array[Selection.Fragment] = js.native

}

@js.native
object Selection extends js.Object{

  @js.native
  class NullSelection extends Selection{
    
    override def isNull(): Boolean = js.native
    override def toJSON(): js.Object = js.native
    override def clone(): NullSelection = js.native

  }

  @js.native
  class Fragment(val path: js.Any, val startOffset: Int, val endOffset: Int, val full: Boolean) extends js.Object{
    
    def isAnchor(): Boolean = js.native
    def isInline(): Boolean = js.native
    def isPropertyFragment(): Boolean = js.native
    def isNodeFragment(): Boolean = js.native
    def isFull(): Boolean = js.native
    def isPartial(): Boolean = js.native
    def getNodeId(): js.Any = js.native

  }

  @js.native
  class NodeFragment extends js.Object{

    def isAnchor(): Boolean = js.native
    def isInline(): Boolean = js.native
    def isPropertyFragment(): Boolean = js.native
    def isNodeFragment(): Boolean = js.native
    def isFull(): Boolean = js.native
    def isPartial(): Boolean = js.native
    def getNodeId(): js.Any = js.native
  }

  @js.native
  class Cursor(path: js.Array[String], offset: Int) extends Anchor{
    def isPropertyFragment(): Boolean = js.native
    def isNodeFragment(): Boolean = js.native
  }

  def fromJSON(json: js.Object): js.Object = js.native
  def create(): Unit = js.native

}

