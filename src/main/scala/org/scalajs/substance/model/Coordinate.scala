package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Coordinate(path: js.Array[String], offset: Int, after: Option[js.Any]) extends js.Object{

  def equals(other: js.Any): Boolean = js.native
  def withCharPos(offset: Int): Coordinate = js.native
  def getNodeId(): js.Any = js.native
  def getPath(): js.Any = js.native
  def getOffset(): Int = js.native
  def toJSON(): js.Object = js.native
  override def toString(): String = js.native
  def isPropertyCoordinate(): Boolean = js.native
  def isNodeCoordinate(): Boolean = js.native

}
