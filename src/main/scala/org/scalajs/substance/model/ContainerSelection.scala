package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ContainerSelection(val containerId: js.Any, val startPath: js.Any, val startOffset: js.Any,
                         val endPath: js.Any, val endOffset: js.Any, val reverse: js.Any,
                         val surfaceId: js.Any) extends Selection{

  override def toJSON(): js.Object = js.native
  override def isContainerSelection(): Boolean = js.native
  override def isNodeSelection(): Boolean = js.native
  override def isNull(): Boolean = js.native
  override def isCollapsed(): Boolean = js.native
  override def isReverse(): Boolean = js.native
  override def equals(other: Selection): Boolean = js.native
  override def toString(): String = js.native
  def getContainer(): Container = js.native
  def isInsideOf(other: ContainerSelection, strict: js.Any): Boolean = js.native
  def contains(other: ContainerSelection, strict: js.Any): Boolean = js.native
  def overlaps(other: ContainerSelection): Boolean = js.native
  def isLeftAlignedWith(other: ContainerSelection): Boolean = js.native
  def isRightAlignedWith(other: ContainerSelection): Boolean = js.native
  def containsNode(nodeId: js.Any): Boolean = js.native
  def collapse(direction: js.Any): ContainerSelection = js.native
  def expand(other: ContainerSelection): ContainerSelection = js.native
  def truncateWith(other: ContainerSelection): ContainerSelection = js.native
  override def getFragments(): js.Array[Selection.Fragment] = js.native
  def splitIntoPropertySelections(): js.Array[PropertySelection] = js.native

}

@js.native
object ContainerSelection extends js.Object{

  def fromJSON(properties: js.Object): ContainerSelection = js.native

}
