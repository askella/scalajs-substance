package org.scalajs.substance.model

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Container(arguments: js.Any*) extends DocumentNode(???, ???){

  @JSName("dispose")
  def disposeM(): Unit = js.native
  def getPosition(nodeId: js.Any): Int = js.native
  def getNodes(): js.Array[data.Node] = js.native
  def show(nodeId: js.Any, pos: Int): Unit = js.native
  def hide(nodeId: js.Any): Unit = js.native
  def getAddress(coor: Coordinate): ContainerAddress = js.native
  def getChildrenProperty(): String = js.native
  def getLength(): Int = js.native

}
