package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Fragmenter(options: js.Object) extends js.Object{

  def start(rootContext: js.Any, text: js.Any, annotations: js.Any): js.Any = js.native
  def onText(context: js.Any, text: js.Any, entry: js.Any): Unit = js.native
  def onEnter(entry: js.Any, parentContext: js.Any): js.Any = js.native
  def onExit(entry: js.Any, context: js.Any, parentContext: js.Any): js.Any = js.native

}
