package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ContainerAddress(val pos: Int, val offset: Int) extends js.Object{

  def isBefore(other: js.Any, strict: js.Any): Boolean = js.native
  def isAfter(other: js.Any, strict: js.Any): Boolean = js.native
  def isEqual(other: js.Any): Boolean = js.native
  override def toString(): String = js.native

}
