package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TransactionDocument(document: Document, session: js.Any, schema: DocumentSchema) extends Document(schema){

  def reset(): Unit = js.native
  def create(nodeData: js.Any): js.Any = js.native
  def delete(nodeId: js.Any): js.Any = js.native
  def set(path: js.Any, value: js.Any): js.Any = js.native
  def update(path: js.Any, diffOp: js.Any): js.Any = js.native
  def cancel(): Unit = js.native
  def getOperations(): js.Any = js.native
  override def newInstance(): Document = js.native
  val isTransactionDocument: Boolean = js.native


}
