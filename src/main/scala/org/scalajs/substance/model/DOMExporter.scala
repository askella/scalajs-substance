package org.scalajs.substance.model

import org.scalajs.substance.ui.DOMElement

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DOMExporter(val config: js.Any) extends js.Object{

  def exportDocument(doc: js.Any): js.Any = js.native
  def convertDocument(doc: js.Any): js.Any = js.native
  def convertContainer(container: js.Any): js.Array[DOMElement] = js.native
  def convertNode(node: js.Any): DOMElement = js.native
  def convertProperty(doc: js.Any, path: js.Any, options: js.Any): String = js.native
  def annotatedText(path: js.Any): js.Array[js.Any] = js.native
  def getNodeConverter(node: js.Any): js.Any = js.native
  def getDefaultBlockConverter(): js.Object = js.native
  def getDefaultPropertyAnnotationConverter(): js.Object = js.native
  def getDocument(): js.Any = js.native
  def createElement(str: String): js.Any = js.native


}
