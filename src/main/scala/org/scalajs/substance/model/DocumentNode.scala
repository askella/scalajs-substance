package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentNode(doc: js.Any, props: js.Object) extends data.Node(props){

  def getDocument(): DocumentNode = js.native
  def hasParent(): Boolean = js.native
  def getParent(): DocumentNode = js.native
  def hasChildren(): Boolean = js.native
  def getChildIndex(): Int = js.native
  def getChildAt(idx: js.Any): Option[DocumentNode] = js.native
  def getChildCount(): Int = js.native
  def getRoot(): DocumentNode = js.native
  def getAddressablePropertyNames(): js.Array[String] = js.native
  def hasAddressableProperties(): Boolean = js.native
  def getPropertyNameAt(idx: Int): js.Any = js.native
  def setHighlighted(highlighted: js.Any, scope: js.Any): Unit = js.native
  def on(eventName: js.Any, handler: js.Any, ctx: js.Any): Unit = js.native
  def off(ctx: js.Any, eventName: js.Any, handler: js.Any): Unit = js.native
  def connect(ctx: js.Any, handler: js.Any): Unit = js.native
  def disconnect(ctx: js.Any): Unit = js.native
  def isBlock(): Boolean = js.native
  def isText(): Boolean = js.native
  def isPropertyAnnotation(): Boolean = js.native
  def isInline(): Boolean = js.native
  def isContainerAnnotation(): Boolean = js.native


}
