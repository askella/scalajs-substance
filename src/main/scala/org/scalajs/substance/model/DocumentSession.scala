package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentSession(val doc: js.Any, options: js.Object) extends js.Object{

  def getDocument(): js.Any = js.native
  def getSelection(): Selection = js.native
  def setSelection(sel: Selection): Unit = js.native
  def getCollaborators(): js.Any = js.native
  def canUndo(): Boolean = js.native
  def canRedo(): Boolean = js.native
  def undo(): Unit = js.native
  def redo(): Unit = js.native
  def transaction(transformation: js.Any, info: js.Any): Unit = js.native
  def onDocumentChange(change: js.Any, info: js.Any): Unit = js.native
  def _transformLocalChangeHistory(externalChange: js.Any): Unit = js.native
  def _transformSelection(change: js.Any): Unit = js.native
  def _transformCollaboratorSelections(change: js.Any): Unit = js.native
  def afterDocumentChange(): Unit = js.native
  def _commit(change: js.Any, info: js.Any): Unit = js.native
  def _notifyChangeListeners(change: js.Any, info: js.Any): Unit = js.native


}
