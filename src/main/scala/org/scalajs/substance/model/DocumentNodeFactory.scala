package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentNodeFactory(val doc: js.Any) extends js.Object{

}

@js.native
object DocumentNodeFactory extends js.Object{

  def create(nodeType: js.Any, nodeData: js.Any): js.Any = js.native

}