package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class InlineWrapper extends InlineNode{

}

@js.native
object InlineWrapper extends js.Object{

  val name: String = js.native

}