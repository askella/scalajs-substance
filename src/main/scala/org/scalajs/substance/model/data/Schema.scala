package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Schema(val name: String, val version: String) extends js.Object{

  def addNodes(nodes: js.Array[Node]): Unit = js.native
  def addNode(nodeClass: js.Any): Unit = js.native
  def getNodeClass(name: String): js.Any = js.native
  def getBuiltIns(): js.Array[Node] = js.native
  def isInstanceOf(`type`: String, parentType: String): Boolean = js.native
  // TODO: define each method here
  def getTocTypes(): js.Array[Node] = js.native
  def getDefaultTextType(): String = js.native
  def getNodeSchema(`type`: js.Any): Unit = js.native


}
