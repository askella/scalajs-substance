package org.scalajs.substance.model.data

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ArrayOperation(val data: js.Any) extends js.Object{

  @JSName("apply")
  def applyM(array: js.Array[js.Any]): js.Array[js.Any] = js.native
  @JSName("clone")
  def cloneM(): ArrayOperation = js.native
  def invert(): ArrayOperation = js.native
  def hasConflict(other: js.Any): Boolean = js.native
  def toJSON(): js.Object = js.native
  def isInsert(): Boolean = js.native
  def isDelete(): Boolean = js.native
  def getOffset(): js.Any = js.native
  def getValue(): js.Any = js.native
  def isNOP(): Boolean = js.native
  override def toString(): String = js.native

}
// TODO: define static methods here