package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Data(val schema: Schema, val options: Option[js.Object]) extends js.Object{

  def contains(id: Int): Boolean = js.native
  def get(path: js.Any): js.Any = js.native
  def getNodes(): DataObject = js.native
  def create(nodeData: js.Any): Node = js.native
  def delete(nodeId: String): Node = js.native
  def set(path: js.Array[String], newValue: js.Object): js.Any = js.native
  @deprecated("use set", "1.0")
  def update(path: js.Array[String], diff: js.Object): js.Any = js.native
  def toJSON(): js.Object = js.native
  def reset(): Unit = js.native
  def addIndex(name: String, index: NodeIndex): Unit = js.native
  def getIndex(name: String): NodeIndex = js.native
  def _updateIndexes(change: js.Object): Unit = js.native
  def _stopIndexing(): Unit = js.native
  def _startIndexing(): Unit = js.native

}
