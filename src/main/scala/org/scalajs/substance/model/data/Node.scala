package org.scalajs.substance.model.data

import org.scalajs.substance.util.EventEmitter

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Node(props: js.Object) extends EventEmitter{

  def dispose(): Unit = js.native
  def isInstanceOf(typeName: String): Boolean = js.native
  def getTypeNames(): js.Array[String] = js.native
  def getPropertyType(propertyName: String): js.Any = js.native
  def toJSON(): js.Object = js.native

}

@js.native
object Node extends Node(???){
  def defineSchema(schema: js.Object): Unit = js.native
}
