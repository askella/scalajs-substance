package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class NodeIndex extends js.Object{

  def get(path: js.Array[String]): js.Any = js.native
  def getAll(path: js.Array[String]): js.Any = js.native
  def select(node: js.Any): Boolean = js.native
  def create(node: js.Any): Unit = js.native
  def delete(node: js.Any): Unit = js.native
  def update(node: js.Any, path: js.Any, newValue: js.Any, oldValue: js.Any): Unit = js.native
  def set(node: js.Any, path: js.Any, newValue: js.Any, oldValue: js.Any): Unit = js.native

}
// TODO: define static methods here