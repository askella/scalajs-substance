package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class OperationSerializer extends js.Object{

  def serialize(op: js.Any): js.Array[String] = js.native
  def serializePrimitiveOp(op: js.Any): js.Array[String] = js.native
  def deserialize(str: String, tokenizer: js.Any): js.Any = js.native
  def deserializePrimitiveOp(str: String, tokenizer: js.Any): js.Any = js.native


}

@js.native
class Tokenizer(val str: js.Any, val sep: String) extends js.Object{

  def error(msg: js.Any): Unit = js.native
  def getString(): String = js.native
  def getNumber(): Int = js.native
  def getObject(): js.Any = js.native
  def getAny(): js.Any = js.native
  def getPath(): js.Array[String] = js.native

}