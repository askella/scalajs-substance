package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
// complete
@js.native
class DataObject(val root: Option[js.Object]) extends js.Object{

  def getRoot(): js.Any = js.native
  def get(path: js.Any): js.Any = js.native
  def set(path: js.Any, value: js.Any): Unit = js.native
  def delete(path: js.Any): Unit = js.native
  def clear(): Unit = js.native

}
