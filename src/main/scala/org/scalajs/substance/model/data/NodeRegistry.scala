package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class NodeRegistry extends js.Object{

  def register(nodeClazz: String): Unit = js.native

}
