package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class NodeFactory(val nodeRegistry: NodeRegistry) extends js.Object{

  def create(nodeType: js.Any, nodeData: js.Any): js.Any = js.native

}
