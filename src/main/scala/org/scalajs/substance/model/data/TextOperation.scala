package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TextOperation(val data: js.Any) extends js.Object{

  // TODO: define clone and apply here
  def isNOP(): Boolean = js.native
  def isInsert(): Boolean = js.native
  def isDelete(): Boolean = js.native
  def getLength(): Int = js.native
  def invert(): TextOperation = js.native
  def hasConflict(other: js.Any): Boolean = js.native
  def isEmpty(): Boolean = js.native
  def toJSON(): js.Object = js.native
  override def toString(): String = js.native

}
// TODO: define static methods here