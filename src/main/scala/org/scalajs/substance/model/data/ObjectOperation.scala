package org.scalajs.substance.model.data

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ObjectOperation extends js.Object{

  // TODO: define clone and apply here
  def isNOP(): Boolean = js.native
  def isCreate(): Boolean = js.native
  def isDelete(): Boolean = js.native
  def isUpdate(): Boolean = js.native
  def isSet(): Boolean = js.native
  def invert(): ObjectOperation = js.native
  def hasConflict(other: js.Any): Boolean = js.native
  def toJSON(): js.Object = js.native
  def getType(): js.Any = js.native
  def getPath(): js.Any = js.native
  def getValue(): js.Any = js.native
  def getOlcValue(): js.Any = js.native
  def getValueOp(): js.Any = js.native
  override def toString(): String = js.native

}
// TODO: define static methods here
