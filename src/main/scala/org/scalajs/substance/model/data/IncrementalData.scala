package org.scalajs.substance.model.data

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class IncrementalData(val schema: Schema, val options: Option[js.Object]) extends js.Object{

  def create(nodeData: js.Object): ObjectOperation = js.native
  def delete(nodeId: String): ObjectOperation = js.native
  def update(path: js.Array[String], diff: js.Object): ObjectOperation = js.native
  def set(path: js.Array[String], newValue: js.Object): ObjectOperation = js.native
  @JSName("apply")
  def applyM(op: ObjectOperation): Unit = js.native
  def _getDiffOp(path: js.Array[String], diff: js.Object): ObjectOperation = js.native

}
