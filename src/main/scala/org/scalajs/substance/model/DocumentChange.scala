package org.scalajs.substance.model

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DocumentChange(val ops: js.Any, val before: js.Any, val after: js.Any) extends js.Object{

  def _extractInformation(doc: js.Any): Unit = js.native
  def invert(): js.Any = js.native
  def isAffected(path: js.Any): Boolean = js.native
  def serialize(): String = js.native
  @JSName("clone")
  def cloneM(): DocumentChange = js.native
  def toJSON(): js.Object = js.native
  
}

@js.native
object DocumentChange extends js.Object{

  def transformInplace(A: js.Any, B: js.Any): Unit = js.native
  def transformSelection(coor: Coordinate, op: js.Any): Boolean = js.native

}
