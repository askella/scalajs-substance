package org.scalajs.substance.model

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DefaultChangeCompressor extends js.Object{

  def shouldMerge(lastChange: js.Any, newChange: js.Any): Boolean = js.native
  def merge(first: DocumentChange, second: DocumentChange): Boolean = js.native
  
}
