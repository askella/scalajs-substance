package org.scalajs.substance.packages.embed

import org.scalajs.substance.ui.SurfaceTool
import org.scalajs.substance.ui.{Component, SurfaceTool}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class EmbedTool extends SurfaceTool{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def createEmbed(src: String): Unit = js.native
  def onClick(): Unit = js.native

}

@js.native
object EmbedTool extends EmbedTool{
  val name: String = js.native
}

@js.native
class URLPrompt extends Component{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def onSave(): Unit = js.native

}

