package org.scalajs.substance.packages.embed

import org.scalajs.substance.model.BlockNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Embed extends BlockNode(???){

}

@js.native
object Embed extends Embed{
  val name: String = js.native
}