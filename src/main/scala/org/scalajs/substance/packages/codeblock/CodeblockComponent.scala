package org.scalajs.substance.packages.codeblock

import org.scalajs.substance.ui.Component

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
@JSName("Codeblock")
class CodeblockComponent extends Component{
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native

}
            