package org.scalajs.substance.packages.codeblock

import org.scalajs.substance.model.TextBlock

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Codeblock extends TextBlock(???, ???){

}

@js.native
object Codeblock extends Codeblock{
  val name: String = js.native
}
