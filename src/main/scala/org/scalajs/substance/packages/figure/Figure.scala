package org.scalajs.substance.packages.figure

import org.scalajs.substance.model.DocumentNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Figure extends DocumentNode(???, ???){

}

@js.native
object Figure extends Figure{

  val name: String = js.native

}
