package org.scalajs.substance.packages.figure

import org.scalajs.substance.ui.SurfaceCommand

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class InsertFigureCommand extends SurfaceCommand(???){

  def getCommandState(): js.Object = js.native
  override def execute(): js.Any = js.native

}
            