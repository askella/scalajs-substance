package org.scalajs.substance.packages.list

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class ListEditing extends js.Object{

  def register(behavior: js.Any): Unit = js.native
  def breakList(tx: js.Any, args: js.Any*): js.Array[js.Any] = js.native
  def mergeListWithList(tx: js.Any, args: js.Any*): js.Array[js.Any] = js.native
  def mergeListWithTextish(tx: js.Any, args: js.Any*): js.Array[js.Any] = js.native
  def mergeTextishWithList(tx: js.Any, args: js.Any*): js.Array[js.Any] = js.native
  def mergeListItems(tx: js.Any, args: js.Any*): js.Array[js.Any] = js.native

}
            