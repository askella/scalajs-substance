package org.scalajs.substance.packages.paragraph

import org.scalajs.substance.model.TextBlock

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Paragraph extends TextBlock(???, ???){

}

@js.native
object Paragraph extends Paragraph{
  val name: String = js.native
}

