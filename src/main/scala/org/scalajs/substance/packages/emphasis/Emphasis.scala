package org.scalajs.substance.packages.emphasis

import org.scalajs.substance.model.PropertyAnnotation

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Emphasis extends PropertyAnnotation(???){

}

@js.native
object Emphasis extends Emphasis{
  val name: String = js.native
  val fragmentation: js.Any = js.native
}

            