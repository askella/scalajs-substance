package org.scalajs.substance.packages.emphasis

import org.scalajs.substance.ui.AnnotationTool

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class EmphasisTool extends AnnotationTool{

}

@js.native
object EmphasisTool extends EmphasisTool{
  val name: String = js.native
}
            