package org.scalajs.substance.packages.emphasis

import org.scalajs.substance.ui.AnnotationCommand

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class EmphasisCommand extends AnnotationCommand(???){


}

@js.native
object EmphasisCommand extends EmphasisCommand{

  val name: String = js.native
  val annotationType: String = js.native

}
            