package org.scalajs.substance.packages.proseeditor

import org.scalajs.substance.ui.VirtualElement
import org.scalajs.substance.ui.{Controller, VirtualElement}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class ProseEditor extends Controller{

  override def didMount(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): VirtualElement = js.native

}
            