package org.scalajs.substance.packages.code

import org.scalajs.substance.ui.SurfaceTool

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class CodeTool extends SurfaceTool{

}

@js.native
object CodeTool extends CodeTool{
  val name: String = js.native
}

            