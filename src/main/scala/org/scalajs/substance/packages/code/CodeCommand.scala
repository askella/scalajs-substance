package org.scalajs.substance.packages.code

import org.scalajs.substance.ui.AnnotationCommand

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class CodeCommand extends AnnotationCommand(???){

}

@js.native
object CodeCommand extends CodeCommand{
  val name: String = js.native
  val annotationType: String = js.native
}
            