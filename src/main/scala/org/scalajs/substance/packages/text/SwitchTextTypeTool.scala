package org.scalajs.substance.packages.text

import org.scalajs.substance.ui.SurfaceTool

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class SwitchTextTypeTool extends SurfaceTool{

  override def didMount(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  override def getInitialState(): js.Object = js.native
  override def didUpdate(): Unit = js.native
  def executeCommand(textType: SwitchTextType): Unit = js.native
  def getTextCommands(): js.Any = js.native
  def handleClick(e: js.Any): Unit = js.native
  def onKeydown(event: js.Any): Unit = js.native
  def toggleAvailableTextTypes(e: js.Any): Unit = js.native
  def toggleDropdown(): Unit = js.native

}

@js.native
object SwitchTextTypeTool extends SwitchTextTypeTool{
  val name: String = js.native
}

            