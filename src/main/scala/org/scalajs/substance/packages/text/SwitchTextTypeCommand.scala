package org.scalajs.substance.packages.text

import org.scalajs.substance.model.Selection
import org.scalajs.substance.ui.{Surface, SurfaceCommand}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
* Created by rakshith on 22/6/16.
*/
@js.native
class SwitchTextType extends SurfaceCommand(???){
  override def getSelection(): Selection = js.native
  def getTextTypes(): js.Array[js.Object] = js.native
  def getTextType(textTypeName: String): js.Object = js.native
  def getCurrentTextType(node: js.Any): js.Object = js.native
  def getCommandState(): js.Object = js.native
  def execute(textTypeName: String): js.Object = js.native
}

@js.native
object SwitchTextType extends SwitchTextType{
  val name: String = js.native
}
            