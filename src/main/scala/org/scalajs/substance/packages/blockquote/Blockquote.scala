package org.scalajs.substance.packages.blockquote

import org.scalajs.substance.model.TextBlock
import org.scalajs.substance.ui.Component

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Blockquote extends TextBlock(???, ???){

}

@js.native
object Blockquote extends Blockquote{
  val name: String = js.native
}
            