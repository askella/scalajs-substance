package org.scalajs.substance.packages.include

import org.scalajs.substance.model.BlockNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Include extends BlockNode(???){

}

@js.native
object Include extends Include{
  val name: String = js.native
}

            