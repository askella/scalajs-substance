package org.scalajs.substance.packages.strong

import org.scalajs.substance.ui.SurfaceTool

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class StrongTool extends SurfaceTool{

}

@js.native
object StrongTool extends StrongTool{
  val name: String = js.native
}
            