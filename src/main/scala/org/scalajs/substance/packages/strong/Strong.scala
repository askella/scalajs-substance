package org.scalajs.substance.packages.strong

import org.scalajs.substance.model.PropertyAnnotation

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Strong extends PropertyAnnotation{

}

@js.native
object Strong extends Strong{

  val name: String = js.native
}

            