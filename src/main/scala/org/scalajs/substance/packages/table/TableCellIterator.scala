package org.scalajs.substance.packages.table

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class TableCellIterator(tableNode: Table) extends js.Object{

  def next(): Option[TableCell] = js.native
  def nextSection(it: TableCellIterator): Unit = js.native
  def nextRow(it: TableCellIterator): Unit = js.native
  def nextCell(it: TableCellIterator): Unit = js.native

}
            