package org.scalajs.substance.packages.table

import org.scalajs.substance.model.Selection
import org.scalajs.substance.ui.Component

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class TableComponent extends Component{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  override def getInitialState(): js.Object = js.native
  def renderTableContent(@JSName("$$") dollar: js.Function): js.Any = js.native
  def onDoubleClick(e: js.Any): Unit = js.native
  def onMouseDown(e: js.Any): Unit = js.native
  def onMouseOver(e: js.Any): Unit = js.native
  def onSelectionChange(sel: Selection): Unit = js.native

}
            