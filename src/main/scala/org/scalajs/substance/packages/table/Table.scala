package org.scalajs.substance.packages.table

import org.scalajs.substance.model.BlockNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
* Created by rakshith on 22/6/16.
*/
@js.native
class Table extends BlockNode(???){
  def getChildrenProperty(): String = js.native
  def getSections(): js.Array[js.Any] = js.native
  def getSectionAt(secIdx: Int): js.Any = js.native
  def getMatrix(): js.Any = js.native
  def getIterator(): TableCellIterator = js.native
  def getSize(dimension: js.Any): Int = js.native
  def toTSV(): String = js.native
}

@js.native
object Table extends Table{
  val name: String = js.native
  def fromTSV(tx: js.Any, tsv: js.Any, sep: Char): js.Object = js.native
}
            