package org.scalajs.substance.packages.table

import org.scalajs.substance.model.DocumentNode

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class TableCell extends DocumentNode(???, ???){

  def getSpan(dim: String): Int = js.native
  def isData(): Boolean = js.native
}

@js.native
object TableCell extends TableCell{
  val name: String = js.native
}
            