package org.scalajs.substance.packages.table

import TableMatrix.Rectangle

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class TableMatrix(tableNode: Table) extends js.Object{

  def invalidate(): Unit = js.native
  def update(): Unit = js.native
  def getCell(row: Int, col: Int): TableCell = js.native
  def getColumn(col: Int): js.Array[TableCell] = js.native
  def getRow(row: Int): js.Array[TableCell] = js.native
  def getRowNode(row: Int): TableRow = js.native
  def getRowNodes(): js.Array[TableRow] = js.native
  def getRectangle(startCellNode: TableCell, endCellNode: TableCell): Rectangle = js.native
  def getCellsForRectangle (rect: Rectangle): js.Array[TableCell] = js.native
  def getBoundingRectangle (rect: Rectangle): Rectangle = js.native
  def getSize (): js.Object = js.native
  def lookupCell(cellNode: TableCell): Option[TableCell] = js.native
  def findClosestCell(cell: TableCell): Option[TableCell] = js.native
  
}

@js.native
object TableMatrix extends TableMatrix(???){

  @js.native
  class Cell(node: TableCell, row: Int, col: Int) extends js.Object{

  }
  @js.native
  object Cell extends  js.Object{
    def sortDescending(a: Cell, b: Cell): Int = js.native
  }

  @js.native
  class PlaceHolder(owner: TableCell, row: Int, col: Int) extends Cell(???, ???, ???){

  }

  @js.native
  class Rectangle(minRow: Int, minCol: Int, maxRow: Int, maxCol: Int) extends js.Object{

  }
  @js.native
  object Rectangle extends js.Object{
    def copy(rect: Rectangle): Rectangle = js.native
  }

}

            