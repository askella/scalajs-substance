package org.scalajs.substance.packages.link

import org.scalajs.substance.model.PropertyAnnotation

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Link extends PropertyAnnotation(???){

}

@js.native
object Link extends Link{
  val name: String = js.native
  val fragmentation: js.Any = js.native
}

