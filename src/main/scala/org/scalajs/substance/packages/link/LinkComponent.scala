package org.scalajs.substance.packages.link

import org.scalajs.substance.ui.AnnotationComponent

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class LinkComponent extends AnnotationComponent{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native

}