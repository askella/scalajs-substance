package org.scalajs.substance.packages.link

import org.scalajs.substance.model.Selection
import org.scalajs.substance.model.{Annotation, Selection}
import org.scalajs.substance.ui.AnnotationCommand

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class LinkCommand extends AnnotationCommand(???){

  override def getAnnotationData(): js.Object = js.native
  override def canEdit(annos: js.Array[Annotation], sel: Selection): Boolean = js.native

}

@js.native
object LinkCommand extends LinkCommand{
  val name: String = js.native
}

