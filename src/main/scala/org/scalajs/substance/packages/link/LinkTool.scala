package org.scalajs.substance.packages.link

import org.scalajs.substance.ui.SurfaceTool
import org.scalajs.substance.ui.{Component, SurfaceTool}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class LinkTool extends SurfaceTool{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def onCommandExecuted(info: js.Any, commandName: String): Unit = js.native
  def togglePrompt(): Unit = js.native
  def updateLink(linkAttrs: js.Object): Unit = js.native
  def deleteLink(): Unit = js.native
  def getLink(): js.Any = js.native

}

@js.native
class EditLinkPrompt extends Component{

  override def didMount(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def onSave(e: js.Any): Unit = js.native
  def onDelete(e: js.Any): Unit = js.native

}
            