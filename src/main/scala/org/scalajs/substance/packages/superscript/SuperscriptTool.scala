package org.scalajs.substance.packages.superscript

import org.scalajs.substance.ui.SurfaceTool

import scala.scalajs.js

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class SuperscriptTool extends SurfaceTool{


}

@js.native
object SuperscriptTool extends SuperscriptTool{
  val name: String = js.native
}

            