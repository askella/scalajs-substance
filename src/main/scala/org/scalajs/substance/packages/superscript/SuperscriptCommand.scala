package org.scalajs.substance.packages.superscript

import org.scalajs.substance.ui.AnnotationCommand

import scala.scalajs.js

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class SuperscriptCommand extends AnnotationCommand(???){

}

@js.native
object SuperscriptCommand extends SuperscriptCommand{
  val name: String = js.native
  val annotationType: js.Object = js.native
}


            