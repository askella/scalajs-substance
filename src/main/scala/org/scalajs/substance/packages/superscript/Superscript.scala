package org.scalajs.substance.packages.superscript

import org.scalajs.substance.model.PropertyAnnotation

import scala.scalajs.js

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Superscript extends PropertyAnnotation{


}

@js.native
object Superscript extends Superscript{

  val name: String = js.native

}

            