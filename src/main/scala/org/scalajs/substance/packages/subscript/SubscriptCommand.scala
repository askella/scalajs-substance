package org.scalajs.substance.packages.subscript

import org.scalajs.substance.ui.AnnotationCommand

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class SubscriptCommand extends AnnotationCommand(???){

}

@js.native
object SubscriptCommand extends SubscriptCommand{
  val name: String = js.native
  val annotationType: js.Any = js.native
}


            