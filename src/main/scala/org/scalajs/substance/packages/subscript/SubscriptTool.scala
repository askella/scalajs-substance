package org.scalajs.substance.packages.subscript

import org.scalajs.substance.ui.SurfaceTool

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class SubscriptTool extends SurfaceTool{


}

@js.native
object SubscriptTool extends SubscriptTool{
  val name: String = js.native
}

            