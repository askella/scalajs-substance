package org.scalajs.substance.packages.subscript

import org.scalajs.substance.model.PropertyAnnotation

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
* Created by rakshith on 22/6/16.
*/

@js.native
class Subscript extends PropertyAnnotation{


}

@js.native
object Subscript extends Subscript{

  val name: String = js.native

}

            