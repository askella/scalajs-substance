
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Layout extends Component{
  def render(@JSName("$$") dollar: js.Any): VirtualElement = js.native
}
