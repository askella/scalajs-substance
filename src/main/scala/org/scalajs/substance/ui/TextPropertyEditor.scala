
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TextPropertyEditor(parent: js.Any, props: js.Object) extends Surface{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  override def selectAll(): Unit = js.native

}
