
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Scrollbar extends Component{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def didUpdate(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def updatePositions(): Unit = js.native
  def getScrollableElement(): js.Any = js.native
  def onResize(): Unit = js.native
  def onScroll(): Unit = js.native
  def onMouseDown(e: js.Any): Unit = js.native
  def onMouseUp(): Unit = js.native
  def onMouseMove(e: js.Any): Unit = js.native

}
