
package org.scalajs.substance.ui

import org.scalajs.substance.model.Selection
import org.scalajs.substance.model.{Coordinate, Selection}
import org.scalajs.substance.model.data.Node

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DOMSelection(surface: Surface) extends js.Object{

  def getSelection(options: js.Object): Selection = js.native
  def setSelection(sel: Selection): Unit = js.native
  def mapDOMRange(wRange: Range): Range = js.native
  def mapDOMSelection(options: js.Object): Range = js.native
  def clear(): Unit = js.native
  def getContainerId(): js.Any = js.native
  def _getRange(anchorNode: Node, anchorOffset: Int, focusNode: Node, focusOffset: Int): Range = js.native
  def _getCoordinate(node: Node, offset: Int, options: js.Object): Coordinate = js.native
  def _searchForCoordinate(node: Node, offset: Int, options: js.Object): Coordinate = js.native
  def _getEnclosingRange(wRange: Range): Range = js.native

}
