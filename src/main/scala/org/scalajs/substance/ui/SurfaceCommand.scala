
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Document, Selection}

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class SurfaceCommand(context: js.Any) extends Command{

  def getSurface(): Surface = js.native
  def getSelection(): Selection = js.native
  def getContainerId(): String = js.native
  def getDocument(): Document = js.native
  override def execute(): js.Any = js.native

}
