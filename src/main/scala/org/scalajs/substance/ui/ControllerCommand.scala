
package org.scalajs.substance.ui

import org.scalajs.substance.model.DocumentSession

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ControllerCommand(context: js.Any) extends Command{

  def getController(): Controller = js.native
  def getDocument(): js.Any = js.native
  def getDocumentSession(): DocumentSession = js.native
  override def execute(): js.Any = js.native

}
