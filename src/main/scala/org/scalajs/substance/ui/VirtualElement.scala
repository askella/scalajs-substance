
package org.scalajs.substance.ui

import VirtualComponent.Outlet

import scala.scalajs.js
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class VirtualElement(owner: js.Any) extends DOMElement{

  override def getParent(): DOMElement = js.native
  def ref(ref: String): VirtualElement = js.native
  
}

@js.native
class VirtualHTMLElement(tagName: String) extends VirtualElement(???){

  override def getTagName(): String = js.native
  override def setTagName(tagName: String): js.Any = js.native
  override def hasClass(className: String): Boolean = js.native
  override def addClass(className: String): VirtualHTMLElement = js.native
  override def removeClass(className: String): VirtualHTMLElement = js.native
  override def removeAttr(attr: String): VirtualHTMLElement = js.native
  override def getAttribute(name: String): String = js.native
  override def setAttribute(name: String, value: String): VirtualHTMLElement = js.native
  override def getAttributes(): js.Any = js.native
  override def getId(): String = js.native
  override def setId(id: String): VirtualHTMLElement = js.native
  override def setTextContent(text: String): VirtualHTMLElement = js.native
  override def setInnerHTML(html: String): VirtualHTMLElement = js.native
  override def getInnerHTML(): String = js.native
  override def getValue(): Option[String] = js.native
  override def setValue(value: String): VirtualHTMLElement = js.native
  override def getChildNodes(): js.Array[DOMElement] = js.native
  override def getChildren(): js.Array[DOMElement] = js.native
  override def isTextNode(): Boolean = js.native
  override def isElementNode(): Boolean = js.native
  override def isCommentNode(): Boolean = js.native
  override def isDocumentNode(): Boolean = js.native
  def append(): VirtualHTMLElement = js.native
  def appendChild(child: DOMElement): VirtualHTMLElement = js.native
  def insertAt(pos: Int, child: DOMElement): VirtualHTMLElement = js.native
  def insertBefore(child: DOMElement, before: DOMElement): VirtualHTMLElement = js.native
  override def removeAt(pos: Int): VirtualHTMLElement = js.native
  def removeChild(child: DOMElement): Unit = js.native
  def replaceChild(oldChild: DOMElement, newChild: DOMElement): Unit = js.native
  override def empty(): VirtualHTMLElement = js.native
  override def getProperty(name: String): Option[String] = js.native
  override def setProperty(name: String, value: String): js.Any = js.native
  override def removeProperty(name: String): Unit = js.native
  override def getStyle(name: String): Option[String] = js.native
  override def setStyle(name: String, value: String): Unit = js.native
  override def addEventListener(eventName: String, handler: js.Function, options: js.Object): Unit = js.native
  override def removeEventListener(eventName: String, handler: js.Function): Unit = js.native
  override def getEventListeners(): js.Array[DOMEventListener] = js.native
  override def getNodeType(): String = js.native
  def hasInnerHTML(): Boolean = js.native

}

@js.native
class VirtualComponent(ComponentClass: js.Any, props: js.Object, tagName: String) extends VirtualHTMLElement(tagName){

  def getComponent(): js.Any = js.native
  override def getChildren(): js.Array[DOMElement] = js.native
  override def getNodeType(): String = js.native
  def outlet(name: String): Outlet = js.native

}

@js.native
object VirtualComponent extends VirtualComponent(???, ???, ???){

  @js.native
  class Outlet(virtualEl: VirtualElement, name: String) extends js.Object{
    def append(): Outlet = js.native
    def empty(): Unit = js.native
  }

}

@js.native
class VirtualTextNode(text: String) extends VirtualElement(???){

}
@js.native
object VirtualElement extends VirtualElement(???){
  def createElement(): VirtualElement = js.native
}



