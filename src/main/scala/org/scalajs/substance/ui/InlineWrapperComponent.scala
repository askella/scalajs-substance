
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class InlineWrapperComponent extends InlineNodeComponent{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native

}
