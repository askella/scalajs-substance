
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
object DefaultDOMElement extends js.Object{

  def createTextNode(text: String): DOMElement = js.native
  def createElement(tagName: String): DOMElement = js.native
  def getBrowserWindow(): js.Any = js.native
  def parseHTML(html: String): js.Any = js.native
  def parseXML(xml: String, fullDoc: js.Any): js.Any = js.native
  def wrapNativeElement(el: js.Any): js.Any = js.native
  
}
