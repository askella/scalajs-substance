
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class BrowserDOMElement(el: js.Any) extends DOMElement{

  override def getNativeElement(): js.Any = js.native
  def hasClass(className: js.Any): js.Any = js.native
  def addClass(className: js.Any): BrowserDOMElement = js.native
  def removeClass(className: js.Any): BrowserDOMElement = js.native
  def getClasses(): String = js.native
  def setClasses(classString: String): Unit = js.native
  override def getAttribute(name: String): String = js.native
  override def setAttribute(name: String, value: String): js.Any = js.native
  override def removeAttribute(name: String): js.Any = js.native
  override def getAttributes(): js.Object = js.native
  override def getProperty(name: String): Option[String] = js.native
  def setProperty(name: String, value: js.Any): Unit = js.native
  override def removeProperty(name: String): Unit = js.native
  override def getTagName(): String = js.native
  override def setTagName(tagName: String): js.Any = js.native
  override def getId(): String = js.native
  override def setId(id: String): js.Any = js.native
  override def getValue(): Option[String] = js.native
  override def setValue(value: String): js.Any = js.native
  override def getStyle(name: String): Option[String] = js.native
  def getComputedStyle(): js.Any = js.native
  def setStyle(name: String, value: js.Any): Unit = js.native
  override def getTextContent(): String = js.native
  override def setTextContent(text: String): js.Any = js.native
  override def getInnerHTML(): String = js.native
  override def setInnerHTML(html: String): js.Any = js.native
  override def getOuterHTML(): String = js.native
  def addEventListener(eventName: String, handler: js.Any, options: js.Any): Unit = js.native
  def _delegatedHandler(listener: js.Any): js.Function = js.native
  def removeEventListener(eventName: String, handler: js.Any): Unit = js.native
  override def getEventListeners(): js.Array[DOMEventListener] = js.native
  override def getChildCount(): Int = js.native
  override def getChildNodes(): js.Array[DOMElement] = js.native
  override def getChildren(): js.Array[DOMElement] = js.native
  override def getChildAt(pos: Int): DOMElement = js.native
  def getChildIndex(child: js.Any): Int = js.native
  override def getFirstChild(): DOMElement = js.native
  override def getLastChild(): DOMElement = js.native
  override def getNextSibling(): DOMElement = js.native
  override def getPreviousSibling(): DOMElement = js.native
  override def isTextNode(): Boolean = js.native
  override def isElementNode(): Boolean = js.native
  override def isCommentNode(): Boolean = js.native
  override def isDocumentNode(): Boolean = js.native
  @JSName("clone")
  override def cloneM(): BrowserDOMElement = js.native
  override def createElement(tagName: String): DOMElement = js.native
  override def createTextNode(text: String): DOMElement = js.native
  override def is(cssSelector: String): Boolean = js.native
  override def getParent(): DOMElement = js.native
  override def getRoot(): DOMElement = js.native
  override def find(cssSelector: String): DOMElement = js.native
  override def findAll(cssSelector: String): js.Array[DOMElement] = js.native
  override def appendChild(child: js.Any): BrowserDOMElement = js.native
  override def insertAt(pos: Int, child: js.Any): js.Any = js.native
  def insertBefore(child: js.Any, before: Int): js.Any = js.native
  override def removeAt(pos: Int): js.Any = js.native
  override def removeChild(child: js.Any): Unit = js.native
  override def replaceChild(oldChild: js.Any, newChild: js.Any): Unit = js.native
  override def empty(): BrowserDOMElement = js.native
  override def remove(): js.Any = js.native
  override def serialize(): String = js.native
  override def isInDocument(): Boolean = js.native
  def _replaceNativeEl(newEl: js.Any): Unit = js.native
  def _getChildNodeCount(): Int = js.native
  override def focus(): BrowserDOMElement = js.native
  override def blur(): BrowserDOMElement = js.native
  override def click(): js.Any = js.native
  override def getWidth(): Int = js.native
  override def getHeight(): Int = js.native
  override def getOffset(): js.Object = js.native
  override def getPosition(): js.Object = js.native
  def getOuterHeight(withMargin: js.Any): Int = js.native

}

@js.native
object BrowserDOMElement extends BrowserDOMElement(???){

  override def createTextNode(text: String): DOMElement = js.native
  override def createElement(tagName: String): DOMElement = js.native
  def parseMarkup(str: String, format: String, isFullDoc: Boolean): js.Any = js.native
  def wrapNativeElement(el: js.Any): js.Any = js.native
  def getBrowserWindow(): BrowserWindow = js.native

  @js.native
  class TextNode(nativeEl: js.Any) extends DOMElement{

  }

  @js.native
  class BrowserWindow extends js.Object{
    def on(arguments: js.Any*): js.Any = js.native
    def off(arguments: js.Any*): js.Any = js.native
    def addEventListener(arguments: js.Any*): js.Any = js.native
    def removeEventListener(arguments: js.Any*): js.Any = js.native
    def getEventListeners(): Unit = js.native
  }

}
