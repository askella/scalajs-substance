
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Toolbar extends Component{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native

}
@js.native
class Dropdown extends Component{

  override def getInitialState(): js.Object = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def handleClick(e: js.Any): Unit = js.native
  def handleDropdownToggle(e: js.Any): Unit = js.native
  def close(): Unit = js.native
  
}

@js.native
class Group extends Component{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native

}
