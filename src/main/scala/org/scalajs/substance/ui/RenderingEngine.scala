
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class RenderingEngine extends js.Object{

}
@js.native
class CaptureContext(owner: js.Any) extends js.Object{

}
@js.native
object RenderingEngine extends RenderingEngine{

  def createContext(): CaptureContext = js.native

}