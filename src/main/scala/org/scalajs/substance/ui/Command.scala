
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Command extends js.Object{

  def execute(): js.Any = js.native
  def getName(): String = js.native

}
