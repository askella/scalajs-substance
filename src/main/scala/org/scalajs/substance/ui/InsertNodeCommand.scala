
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class InsertNodeCommand extends SurfaceCommand(???){

  def getCommandState(): js.Any = js.native
  override def execute(): js.Any = js.native
  def insertNode(tx: js.Any, args: js.Object): js.Any = js.native
  def createNodeData(tx: js.Any, args: js.Object): js.Any = js.native


}
