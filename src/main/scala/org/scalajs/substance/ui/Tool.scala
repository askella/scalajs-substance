
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Tool extends Component{

  override def getInitialState(): js.Object = js.native
  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def renderButton(@JSName("$$") dollar: js.Function): js.Any = js.native
  def getTitle(): String  = js.native
  def getController(): Controller = js.native
  def getName(): String = js.native
  def getCommandName(): String = js.native
  def onClick(e: js.Any): Unit = js.native

}
