
package org.scalajs.substance.ui

import org.scalajs.substance.model.Selection
import org.scalajs.substance.model.{Annotation, Selection}

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class AnnotationCommand(surface: Surface) extends SurfaceCommand(???){

  def getAnnotationType(): String = js.native
  def getAnnotationData(): js.Object = js.native
  def isDisabled(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def canEdit(annos: js.Array[Annotation], sel: Selection): Boolean = js.native
  def canCreate(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def canFuse(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def canDelete(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def canExpand(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def canTruncate(annos: js.Array[AnnotationCommand], sel: js.Object): Boolean = js.native
  def getAnnotationsForSelection(): js.Array[AnnotationComponent] = js.native
  override def execute(): js.Object = js.native
  def getCommandState(): js.Object = js.native
  def applyTransform(transformFn: js.Any): js.Object = js.native
  def executeEdit(): js.Object = js.native
  def executeCreate(): js.Object = js.native
  def executeFuse(): js.Object = js.native
  def executeTruncate(): js.Object = js.native
  def executeExpand(): js.Object = js.native
  def executeDelete(): js.Object = js.native

}
