
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.util.EventEmitter

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TOC(controller: Controller) extends EventEmitter{

  def dispose(): Unit = js.native
  def handleDocumentChange(change: js.Any): Unit = js.native
  def computeEntries(): js.Array[js.Object] = js.native
  def getEntries(): js.Array[js.Object] = js.native
  def getDocument(): Document = js.native
  def getConfig(): js.Object = js.native
  def markActiveEntry(scrollPane: ScrollPane): Unit = js.native

}

@js.native
object TOC extends TOC(???){
  val tocTypes: js.Array[String] = js.native
}

