
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class UndoTool extends ControllerTool{

}
@js.native
object UndoTool extends UndoTool{
  val name: String = js.native
}

