
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Document, DocumentSession, Selection}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Controller extends Component{
  
  override def getChildContext(): js.Object = js.native
  override def willReceiveProps(nextProps: js.Object): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): VirtualElement = js.native
  def getConfig(): js.Object = js.native
  def getCommand(commandName: String): ControllerCommand = js.native
  def executeCommand(commandName: String): ControllerCommand = js.native
  def getLogger(): js.Any = js.native
  def getDocument(): Document = js.native
  def getDocumentSession(): DocumentSession = js.native
  def getSurface(name: String): Surface = js.native
  def getFocusedSurface(): Surface = js.native
  def getSelection(): Selection = js.native
  def getContainerId(): js.Any = js.native
  def registerSurface(surface: Surface): Unit = js.native
  def unregisterSurface(surface: Surface): Unit = js.native
  def didFocus(): Unit = js.native
  def transaction(): Unit = js.native
  def onTransactionStarted(tx: js.Any): Unit = js.native
  def handleApplicationKeyCombos(e: js.Any): js.Any = js.native
  def willUpdateState(newState: js.Any): Unit = js.native
  def handleStateUpdate(newState: js.Any): Unit = js.native
  def onDocumentChanged(change: js.Any, info: js.Any): Unit = js.native
  def onSelectionChanged(sel: Selection, surface: Surface): Unit = js.native
  def onCommandExecuted(info: js.Any, commandName: String, cmd: Command): Unit = js.native
  def _onSelectionChanged(sel: Selection, surface: Surface): Unit = js.native
  def uploadFile(file: js.Any, cb: js.Any): js.Any  = js.native
  def pushState(): Unit = js.native
  def popState(): Unit = js.native
  def saveDocument(): Unit = js.native

}

@js.native
object Controller extends Controller{
  def mergeConfig(one: js.Any, other: js.Any): js.Any = js.native
}
