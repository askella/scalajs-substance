
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Document, DocumentSession, Selection}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Surface extends Component{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def getChildContext(): js.Object = js.native
  def getName(): String = js.native
  def isEditable(): Boolean = js.native
  def isSelectable(): Boolean = js.native
  def isReadonly(): Boolean = js.native
  def getCommand(commandName: String): Command = js.native
  def executeCommand(commandName: String, args: js.Object): Unit = js.native
  def getElement(): js.Any = js.native
  def getController(): Controller = js.native
  def getDocument(): Document = js.native
  def getDocumentSession(): DocumentSession = js.native
  def enable(): Unit = js.native
  def disable(): Unit = js.native
  def isEnabled(): Boolean = js.native
  def isContainerEditor(): Boolean = js.native
  def transaction(transformation: js.Function): js.Any = js.native
  def setFocused(): Unit = js.native
  def getSelection(): Selection = js.native
  def setSelection(sel: Selection): Unit = js.native
  def setSelectionFromEvent(evt: js.Any): Unit = js.native
  def rerenderDomSelection(): Unit = js.native
  def getDomNodeForId(nodeId: String): js.Any = js.native
  def selectAll(): Unit = js.native
  def insertText(tx: js.Any, args: js.Object): js.Any = js.native
  def delete(tx: js.Any, args: js.Object): js.Any = js.native
  def break(tx: js.Any, args: js.Object): js.Any = js.native
  def softBreak(tx: js.Any, args: js.Object): js.Any = js.native
  def copy(doc: Document, selection: Selection): js.Any = js.native
  def paste(tx: js.Any, args: js.Object): js.Any = js.native
  def onDocumentChange(change: js.Any, info: js.Any): Unit = js.native
  def onSelectionChange(): Unit = js.native
  def onCollaboratorsChange(): Unit = js.native
  def onKeyDown(event: js.Any): Unit = js.native
  def onTextInput(event: js.Any): Unit = js.native
  def onCompositionStart(): Unit = js.native
  def onTextInputShim(event: js.Any): Unit = js.native
  def onMouseDown(event: js.Any): Unit = js.native
  def onMouseUp(): Unit = js.native
  def onDomMutations(): Unit = js.native
  def onDragStart(event: js.Any): Unit = js.native
  def onNativeBlur(): Unit = js.native
  def onNativeFocus(): Unit = js.native
  def getBoundingRectangleForSelection(): js.Any = js.native
  
}

@js.native
object Surface extends Surface{
  def getDOMRangeFromEvent(evt: js.Any): Range = js.native
}
