
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ToolManager(controller: Controller) extends js.Object{

  def dispose(): Unit = js.native
  def getCommandState(tool: Tool): js.Any  = js.native
  def registerTool(tool: Tool): Unit = js.native
  def unregisterTool(tool: Tool): Unit = js.native
  def getCommand(tool: Tool): Command = js.native
  def updateTools(): Unit = js.native
  
}
