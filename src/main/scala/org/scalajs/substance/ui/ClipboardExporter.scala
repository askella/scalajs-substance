
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Document, HTMLExporter}

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ClipboardExporter extends HTMLExporter(???){
  
  def exportDocument(doc: Document): String = js.native
  def convertDocument(doc: Document): js.Array[DOMElement] = js.native

}