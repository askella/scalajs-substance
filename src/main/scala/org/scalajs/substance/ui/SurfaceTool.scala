
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class SurfaceTool extends Tool{

  def getCommand(): Command = js.native
  def getSurface(): Surface = js.native
  def getDocument(): Document = js.native
  def getContainer(): js.Any = js.native
  def performAction(): Unit = js.native

}
