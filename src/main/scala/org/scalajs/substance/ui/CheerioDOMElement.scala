
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class CheerioDOMElement(el: js.Any) extends DOMElement{

  override def getNativeElement(): js.Any = js.native
  def _wrapNativeElement(el: js.Any): CheerioDOMElement = js.native
  override def hasClass(className: String): Boolean = js.native
  override def addClass(className: String): CheerioDOMElement = js.native
  override def removeClass(className: String): CheerioDOMElement = js.native
  def getClasses(): js.Any = js.native
  def setClasses(classString: String): Unit = js.native
  override def getAttribute(name: String): String = js.native
  def setAttribute(name: String, value: js.Any): js.Any = js.native
  override def removeAttribute(name: String): js.Any = js.native
  override def getAttributes(): js.Array[String] = js.native
  override def getProperty(name: String): Option[String] = js.native
  def setProperty(name: String, value: js.Any): js.Any = js.native
  override def removeProperty(name: String): Unit = js.native
  override def getTagName(): String = js.native
  override def setTagName(tagName: String): js.Any = js.native
  override def getId(): String = js.native
  override def setId(id: String): js.Any = js.native
  override def getTextContent(): String = js.native
  override def setTextContent(text: String): js.Any = js.native
  override def getInnerHTML(): String = js.native
  override def setInnerHTML(html: String): js.Any = js.native
  override def getOuterHTML(): String = js.native
  override def getValue(): Option[String] = js.native
  def setValue(value: js.Any): js.Any = js.native
  override def getStyle(name: String): Option[String] = js.native
  def setStyle(name: String, value: js.Any): js.Any = js.native
  def addEventListener(): Unit = js.native
  def removeEventListener(): Unit = js.native
  override def getEventListeners(): js.Array[DOMEventListener] = js.native
  override def getChildCount(): Int = js.native
  override def getChildNodes(): js.Array[DOMElement] = js.native
  override def getChildren(): js.Array[DOMElement] = js.native
  override def getChildAt(pos: Int): DOMElement = js.native
  def getChildIndex(child: js.Any): Int = js.native
  override def getFirstChild(): DOMElement = js.native
  override def getLastChild(): DOMElement = js.native
  override def getNextSibling(): DOMElement = js.native
  override def getPreviousSibling(): DOMElement = js.native
  override def isTextNode(): Boolean = js.native
  override def isElementNode(): Boolean = js.native
  override def isCommentNode(): Boolean = js.native
  override def isDocumentNode(): Boolean = js.native
  @JSName("clone")
  override def cloneM(): CheerioDOMElement = js.native
  override def createElement(tagName: String): DOMElement = js.native
  override def createTextNode(text: String): DOMElement = js.native
  override def is(cssSelector: String): Boolean = js.native
  override def getParent(): DOMElement = js.native
  override def getRoot(): DOMElement = js.native
  override def find(cssSelector: String): DOMElement = js.native
  override def findAll(cssSelector: String): js.Array[DOMElement] = js.native
  def _normalizeChild(child: js.Any): js.Any = js.native
  override def appendChild(child: js.Any): js.Any = js.native
  override def insertAt(pos: Int, child: js.Any): js.Any = js.native
  override def insertBefore(child: js.Any, before: js.Any): js.Any = js.native
  override def removeAt(pos: Int): js.Any = js.native
  override def removeChild(child: js.Any): Unit = js.native
  override def replaceChild(oldChild: js.Any, newChild: js.Any): Unit = js.native
  override def empty(): CheerioDOMElement = js.native
  override def remove(): CheerioDOMElement = js.native
  def _replaceNativeEl(newEl: js.Any): Unit = js.native
  override def isInDocument(): Boolean = js.native

}

@js.native
object CheerioDOMElement extends CheerioDOMElement(???){

  override def createTextNode(text: String): DOMElement = js.native
  override def createElement(tagName: String): DOMElement = js.native
  def parseMarkup(str: String, format: js.Any): js.Any = js.native
  def wrapNativeElement(el: js.Any): CheerioDOMElement = js.native

}