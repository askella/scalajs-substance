
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ContainerEditor extends Surface{

  def shouldRerender(): Boolean = js.native
  def render(@JSName("$$") dollar: js.Any): js.Any = js.native
  override def isContainerEditor(): Boolean = js.native
  def getContainerId(): String = js.native
  def getContainer(): js.Any = js.native
  def isEmpty(): Boolean = js.native
  override def isEditable(): Boolean = js.native
  def selectFirst(): Unit = js.native
  def extendBehavior(extension: js.Any): Unit = js.native
  def getTextTypes(): js.Array[js.Object] = js.native
  def getTextCommands(): js.Object = js.native
  override def enable(): Unit = js.native
  override def disable(): Unit = js.native
  override def delete(tx: js.Any, args: js.Object): js.Any = js.native
  override def break(tx: js.Any, args: js.Object): js.Any = js.native
  def insertNode(tx: js.Any, args: js.Object): js.Any = js.native
  def switchType(tx: js.Any, args: js.Object): js.Any = js.native
  override def selectAll(): Unit = js.native
  override def paste(tx: js.Any, args: js.Object): js.Any = js.native
  override def insertText(tx: js.Any, args: js.Object): js.Any = js.native
  override def softBreak(tx: js.Any, args: js.Object): js.Any = js.native
  def copy(doc: js.Any, selection: js.Any): js.Any = js.native
  def onDocumentChange(change: js.Any): Unit = js.native
  def onCreateText(e: js.Any): Unit = js.native
  
}
