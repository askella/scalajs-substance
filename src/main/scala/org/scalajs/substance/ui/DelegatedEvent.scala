
package org.scalajs.substance.ui

import org.scalajs.dom.raw.Event

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DelegatedEvent(owner: Component, selectedTarget: DOMElement, originalEvent: Event) extends js.Object{

}
