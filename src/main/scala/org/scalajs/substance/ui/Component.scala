
package org.scalajs.substance.ui

import org.scalajs.substance.model.Selection

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Component extends js.Object{

  def getChildContext(): js.Object = js.native
  def getInitialState(): js.Object = js.native
  def getParent(): js.Any = js.native
  def getRoot(): js.Any = js.native
  def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def mount(el: js.Any): Component = js.native
  def shouldRerender(newProps: js.Any): Boolean = js.native
  def rerender(): Unit = js.native
  def _render(): Unit = js.native
  def triggerDidMount(): Unit = js.native
  def didMount(): Unit = js.native
  def didUpdate(): Unit = js.native
  def isMounted(): Boolean = js.native
  def triggerDispose(): Unit = js.native
  def dispose(): Unit = js.native
  def send(action: String): Boolean = js.native
  def handleActions(actionHandlers: js.Any): Component = js.native
  def handleAction(name: String, handler: js.Function): Unit = js.native
  def getState(): js.Object = js.native
  def setState(newState: js.Object): Unit = js.native
  def extendState(newState: js.Object): Unit = js.native
  def willUpdateState(newState: js.Object): Unit = js.native
  def getProps(): js.Object  = js.native
  def setProps(newProps: js.Object): Unit = js.native
  def _setProps(newProps: js.Object): Unit = js.native
  def extendProps(updatedProps: js.Object): Unit = js.native
  def willReceiveProps(newProps: js.Object): Unit = js.native
  def getChildNodes(): js.Array[Component] = js.native
  def getChildren(): js.Array[Component] = js.native
  def getChildAt(pos: Int): js.Any = js.native
  def find(cssSelector: String): js.Any = js.native
  def findAll(cssSelector: String): js.Any = js.native
  def appendChild(child: js.Any): Unit = js.native
  def insertAt(pos: Int, child: js.Any): Unit = js.native
  def removeAt(pos: Int): Unit = js.native
  def removeChild(child: js.Any): Unit = js.native
  def replaceChild(oldChild: js.Any, newChild: js.Any): Unit = js.native
  def empty(): Component = js.native
  def remove(): Unit = js.native
  def _getContext(): js.Any = js.native
  def addEventListener(): Unit = js.native
  def removeEventListener(): Unit = js.native
  def insertBefore(): Unit = js.native


}

@js.native
object Component extends Component{

  def render(props: js.Any): js.Any = js.native
  def mount(props: js.Any, el: js.Any): js.Any = js.native
  def mount(ComponentClass: js.Any, props: js.Any, el: js.Any): Unit = js.native

  @js.native
  class ElementComponent(val parent: js.Any, val virtualComp: js.Any) extends Component{

  }

  @js.native
  class TextNodeComponent(val parent: js.Any, val virtualComp: js.Any) extends Component{

    def setTextContent(text: String): Unit = js.native

  }

}



