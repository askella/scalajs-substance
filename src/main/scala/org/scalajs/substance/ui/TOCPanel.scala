
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TOCPanel extends Component{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def getDocument(): Document = js.native
  def onTOCUpdated(): Unit = js.native
  def handleClick(e: js.Any): Unit = js.native

}
