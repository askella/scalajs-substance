
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Annotation, Coordinate, Document}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TextPropertyComponent extends AnnotatedTextComponent{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def getPath(): js.Any = js.native
  override def getText(): String = js.native
  override def getAnnotations(): js.Array[Annotation] = js.native
  def setFragments(): js.Any = js.native
  override def getDocument(): Document = js.native
  def getController(): Controller = js.native
  def getSurface(): Surface = js.native
  def isEditable(): Boolean = js.native
  def isReadonly(): Boolean = js.native
  def getDOMCoordinate(charPos: Int): Coordinate = js.native
  
}

@js.native
object TextPropertyComponent extends TextPropertyComponent{
  def getCoordinate(root: js.Any, node: js.Any, offset: Int): Option[Coordinate] = js.native
}


