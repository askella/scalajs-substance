
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class AnnotationComponent extends Component{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  def render(@JSName("$$") dollar: js.Any): VirtualElement = js.native
  def getClassNames(): String = js.native
  def onHighlightedChanged(): Unit = js.native
}
