package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class DOMElement extends js.Object{
  def getNativeElement(): js.Any = js.native
  def hasClass(className: String): Boolean = js.native
  def addClass(classString: String): DOMElement = js.native
  def removeClass(classString: String): DOMElement = js.native
  def attr(name: String, value: String): DOMElement = js.native
  def attr(name: String): String = js.native
  def removeAttr(name: String): DOMElement = js.native
  def getAttribute(name: String): String = js.native
  def setAttribute(name: String, value: String): js.Any = js.native
  def removeAttribute(name: String): js.Any = js.native
  def getAttributes(): js.Any = js.native
  def htmlProp(name: String, value: String): DOMElement = js.native
  def htmlProp(name: String): String = js.native
  def getProperty(name: String): Option[String] = js.native
  def setProperty(name: String, value: String): js.Any = js.native
  def removeProperty(name: String): Unit = js.native
  def getTagName(): String = js.native
  def setTagName(tagName: String): js.Any = js.native
  def getId(): String = js.native
  def setId(id: String): js.Any = js.native
  @JSName("val")
  def value(value: String): DOMElement = js.native
  @JSName("val")
  def value(): String = js.native
  def getValue(): Option[String] = js.native
  def setValue(value: String): js.Any = js.native
  def css(name: String, value: String): DOMElement = js.native
  def css(name: String): Option[String] = js.native
  def getStyle(name: String): Option[String] = js.native
  def setStyle(name: String, value: String): Unit = js.native
  def text(value: String): DOMElement = js.native
  def text(): String = js.native
  def getTextContent(): String = js.native
  def setTextContent(text: String): js.Any = js.native
  def html(value: String): DOMElement = js.native
  def html(): String = js.native
  def getInnerHTML(): String = js.native
  def setInnerHTML(html: String): js.Any = js.native
  def getOuterHTML(): String = js.native
  def on(eventName: String, handler: js.Function, context: js.Any, options: js.Object): DOMElement = js.native
  def off(eventName: String, handler: js.Function): DOMElement = js.native
  def addEventListener(eventName: String, handler: js.Function, options: js.Object): Unit = js.native
  def removeEventListener(eventName: String, handler: js.Function): Unit = js.native
  def getEventListeners(): js.Array[DOMEventListener] = js.native
  def getNodeType(): String = js.native
  def getChildCount(): Int = js.native
  def getChildNodes(): js.Array[DOMElement] = js.native
  def getChildren(): js.Array[DOMElement] = js.native
  def getChildAt(pos: Int): DOMElement = js.native
  def getChildIndex(child: DOMElement): Int = js.native
  def getChildNodeIterator(): js.Any = js.native
  def getLastChild(): DOMElement = js.native
  def getFirstChild(): DOMElement = js.native
  def getNextSibling(): DOMElement = js.native
  def getPreviousSibling(): DOMElement = js.native
  def isTextNode(): Boolean = js.native
  def isElementNode(): Boolean = js.native
  def isCommentNode(): Boolean = js.native
  def isDocumentNode(): Boolean = js.native
  @JSName("clone")
  def cloneM(): DOMElement = js.native
  def createElement(str: String): DOMElement = js.native
  def createTextNode(text: String): DOMElement = js.native
  def is(cssSelector: String): Boolean = js.native
  def getParent(): DOMElement = js.native
  def getRoot(): DOMElement = js.native
  def find(cssSelector: String): DOMElement = js.native
  def findAll(cssSelector: String): js.Array[DOMElement] = js.native
  def append(child: js.Any): DOMElement = js.native
  def appendChild(child: js.Any): js.Any = js.native
  def insertAt(pos: Int, child: js.Any): js.Any = js.native
  def insertBefore(newChild: js.Any, before: js.Any): js.Any = js.native
  def removeAt(pos: Int): js.Any = js.native
  def removeChild(child: js.Any): Unit = js.native
  def replaceChild(oldChild: js.Any, newChild: js.Any): Unit = js.native
  def empty(): DOMElement = js.native
  def remove(): js.Any = js.native
  def serialize(): String = js.native
  def isInDocument(): Boolean = js.native
  def focus(): DOMElement = js.native
  def blur(): DOMElement = js.native
  def click(): js.Any = js.native
  def getWidth(): Int = js.native
  def getHeight(): Int = js.native
  def getOuterHeight(withMargin: Boolean): Int = js.native
  def getOffset(): js.Object = js.native
  def getPosition(): js.Object = js.native

}

@js.native
object DOMElement extends DOMElement{

  def parseHTML(html: String): js.Any = js.native
  def parseXML(xml: String): js.Any = js.native

}

@js.native
class DOMElementDelegator extends DOMElement{

}

@js.native
class DOMEventListener(eventName: String, handler: js.Function, options: js.Object) extends js.Object{

}

@js.native
class TextNode extends DOMElement{

  override def isTextNode(): Boolean = js.native
  override def getNodeType(): String = js.native
  override def isElementNode(): Boolean = js.native
  override def isDocumentNode(): Boolean = js.native
  override def isCommentNode(): Boolean = js.native
  
}


