
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TextPropertyAnnotator extends Surface{

  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  override def isContainerEditor(): Boolean = js.native

}
