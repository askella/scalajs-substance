
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ScrollPane extends Component{

  override def didMount(): Unit = js.native
  override def dispose(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
  def onDocumentChange(): Unit = js.native
  def onHighlightsUpdated(highlights: js.Any): Unit = js.native
  def onScroll(): Unit = js.native
  def getHeight(): Int = js.native
  def getContentHeight(): Int = js.native
  def getContentElement(): js.Any = js.native
  def getScrollableElement(): js.Any = js.native
  def getScrollPosition(): Int = js.native
  def getPanelOffsetForElement(el: js.Any): js.Any = js.native
  def scrollTo(componentId: String): Unit = js.native

}
