
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ControllerTool extends Tool{

  def getDocument(): Document = js.native
  def performAction(): Unit = js.native
  
}
