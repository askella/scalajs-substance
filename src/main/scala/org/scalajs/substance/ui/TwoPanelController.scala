
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class TwoPanelController extends Controller{

  override def getChildContext(): js.Object = js.native
  override def getInitialState(): js.Object = js.native
  override def didMount(): Unit = js.native
  override def didUpdate(): Unit = js.native
  override def render(@JSName("$$") dollar: js.Function): VirtualElement = js.native
  def getContentPanel(): js.Any = js.native
  def switchState(newState: js.Any, options: js.Object): Unit = js.native
  def switchContext(tabId: String, options: js.Object): Unit = js.native
  def tocEntrySelected(nodeId: String): Unit = js.native
  def closeDialog(): Unit = js.native
  def restoreSelection(): Unit = js.native
  override def uploadFile(file: js.Any, cb: js.Any): js.Any  = js.native
  override def handleStateUpdate(newState: js.Any): Unit = js.native

}
