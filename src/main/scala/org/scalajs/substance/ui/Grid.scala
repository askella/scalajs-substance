
package org.scalajs.substance.ui

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Grid extends Component{
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
}
@js.native
class Row extends Component{
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
}
@js.native
class Cell extends Component{
  override def render(@JSName("$$") dollar: js.Function): js.Any = js.native
}


