
package org.scalajs.substance.ui

import scala.scalajs.js
/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Clipboard(val surface: js.Any) extends js.Object{
  def getSurface(): js.Any = js.native
  def attach(el: js.Any): Unit = js.native
  def didMount(): Unit = js.native
  def detach(rootElement: js.Any): Unit = js.native
  def onCopy(event: js.Any): Unit = js.native
  def onCut(event: js.Any): js.Any = js.native
  def onPaste(event: js.Any): Unit = js.native
  def onBeforePasteShim(): Unit = js.native
  def onPasteShim(): Unit = js.native
  def _copy(): js.Object = js.native
  def _pasteHtml(html: js.Any, text: String): Boolean = js.native
  def _getNativeElement(): js.Any = js.native
}
