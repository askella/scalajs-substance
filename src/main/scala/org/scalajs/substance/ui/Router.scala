
package org.scalajs.substance.ui

import org.scalajs.substance.util.EventEmitter

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Router extends EventEmitter{

  def start(): Unit = js.native
  def readRoute(): js.Any = js.native
  def writeRoute(route: String, opts: js.Any): Unit = js.native
  def dispose(): Unit = js.native
  def parseRoute(routeString: String): js.Object = js.native
  def stringifyRoute(route: js.Object): String  = js.native
  def getRouteString(): String = js.native
  def clearRoute(opts: js.Object): Unit = js.native
  
}

@js.native
object Router extends Router{

  def objectToRouteString(obj: js.Object): String = js.native
  def routeStringToObject(routeStr: String): js.Object = js.native

}

