
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Highlights extends js.Object{

  def get(): js.Object = js.native
  def set(highlights: js.Object): Unit = js.native

}
