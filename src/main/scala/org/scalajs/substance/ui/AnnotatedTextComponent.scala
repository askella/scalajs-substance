
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Annotation, Document}

import scala.scalajs.js
import scala.scalajs.js.annotation.JSName

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class AnnotatedTextComponent extends Component{

  def render(@JSName("$$") dollar: js.Any): js.Any = js.native
  def getText(): String = js.native
  def getAnnotations(): js.Array[Annotation] = js.native
  def _renderContent(@JSName("$$") dollar: js.Any): js.Any = js.native
  def _renderTextNode(context: js.Any, text: js.Any): Unit = js.native
  def _renderFragment(@JSName("$$") dollar: js.Any, fragment: js.Any): js.Any = js.native
  def _finishFragment(fragment: js.Any, context: js.Any, parentContext: js.Any): Unit = js.native
  def getDocument(): Document = js.native
  def getComponentRegistry(): js.Any = js.native

}
