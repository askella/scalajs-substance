
package org.scalajs.substance.ui

import org.scalajs.substance.model.Document
import org.scalajs.substance.model.{Document, HTMLImporter}

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class ClipboardImporter(config: js.Any) extends HTMLImporter(???){

  override def importDocument(html: js.Any): js.Any = js.native
  def importFromJSON(jsonStr: String): js.Any = js.native
  def convertBody(body: String): Unit = js.native
  def _wrapInlineElementsIntoBlockElement(childIterator: js.native): js.Any = js.native
  override def createDocument(): Document = js.native

}
