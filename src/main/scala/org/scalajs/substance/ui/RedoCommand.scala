
package org.scalajs.substance.ui

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class Redo extends ControllerCommand(???){
  def getCommandState(): js.Object = js.native
  override def execute(): js.Any = js.native
}
@js.native
object Redo extends Redo{
  val name: String = js.native
}
