package org.scalajs.substance.util

import scala.scalajs.js

/**
  * Created by rakshith on 20/6/16.
  */
@js.native
class ArrayIterator[T](arr: js.Array[T]) extends js.Object{

  def hasNext(): Boolean = js.native
  def next(): T = js.native
  def back(): T = js.native

}
