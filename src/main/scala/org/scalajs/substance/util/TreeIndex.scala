package org.scalajs.substance.util

import scala.scalajs.js
/**
  * Created by rakshith on 20/6/16.
  */
@js.native
class TreeIndex extends js.Object{

  def get(path: js.Array[String]): js.Object = js.native
  def getAll(path: js.Array[String]): js.Array[js.Object] = js.native
  def set(path: js.Array[String], value: js.Object): Unit = js.native
  def delete(path: js.Array[String]): Unit = js.native
  def clear(): Unit = js.native
  def traverse(fn: js.Function): Unit = js.native
  def forEach(fn: js.Function): Unit = js.native
  
}

@js.native
object TreeIndex extends TreeIndex{

  @js.native
  class Arrays extends TreeIndex{
    
    override def get(path: js.Array[String]): js.Object = js.native
    override def set(path: js.Array[String], value: js.Object): Unit = js.native
    def add(path: js.Array[String], value: js.Object): Unit = js.native
    def remove(path: js.Array[String], value: js.Object): Unit = js.native
  }

}