package org.scalajs.substance.util

import scala.scalajs.js

/**
  * Created by rakshith on 20/6/16.
  */
@js.native
class Registry extends js.Object{

  def contains(name: String): Boolean = js.native
  def add(name: String, entry: js.Object): Unit = js.native
  def remove(name: String): Unit = js.native
  def clear(): Unit = js.native
  def get(name: String): js.Object = js.native
  def each(callback: js.Function, ctx: js.Object): Unit = js.native

}
