
package org.scalajs.substance.util

import scala.scalajs.js

/**
  * Created by rakshith on 15/5/16.
  */
@js.native
class EventEmitter extends js.Object{


  def emit(event: String, argumensts: js.Any*): Boolean = js.native
  def byPriorityDescending(a: EventEmitter, b: EventEmitter): Int = js.native
  def connect(obj: js.Object, methods: js.Array[js.Function], options: js.Object): js.Any = js.native
  def disconnect(listener: js.Object): js.Any = js.native
  def on(event: String, method: js.Function, context: js.Object, options: js.Object): Unit = js.native
  def off(event: String, method: js.Function, context: js.Object): Unit = js.native
  def validateMethod(method: js.Function, context: js.Object): Unit = js.native

  
}
