package org.scalajs.substance.util

import scala.scalajs.js
import scala.scalajs.js.Error

/**
  * Created by rakshith on 20/6/16.
  */
@js.native
class SubstanceError(name: String, options: js.Object) extends Error{

  def inspect(): String = js.native
}

@js.native
object SubstanceError extends SubstanceError(???, ???){

  def fromJSON(err: js.Object): SubstanceError = js.native

}